module Main exposing (..)

import Html


cart =
    [ { name = "Lemon", qty = 1, freeQty = 0 }
    , { name = "Apple", qty = 5, freeQty = 0 }
    , { name = "Pear", qty = 10, freeQty = 0 }
    ]


freeQuantity item =
    if item.qty < 5 then
        item
    else if item.qty >= 5 && item.qty < 10 then
        { item | freeQty = 1 }
    else
        { item | freeQty = 3 }


main =
    List.map freeQuantity cart
        |> toString
        |> Html.text
