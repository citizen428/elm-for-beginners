module Main exposing (..)

import Html


type alias CartItem =
    { name : String
    , qty : Int
    , freeQty : Int
    }


cart : List CartItem
cart =
    [ { name = "Lemon", qty = 1, freeQty = 0 }
    , { name = "Apple", qty = 5, freeQty = 0 }
    , { name = "Pear", qty = 10, freeQty = 0 }
    ]


freeQuantity : CartItem -> CartItem
freeQuantity item =
    if item.qty < 5 then
        item
    else if item.qty >= 5 && item.qty < 10 then
        { item | freeQty = 1 }
    else
        { item | freeQty = 3 }


main : Html.Html msg
main =
    List.map freeQuantity cart
        |> toString
        |> Html.text
