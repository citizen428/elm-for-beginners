module Main exposing (..)

import String exposing (split)
import List exposing (length)
import Html


wordCount =
    split " " >> length


main =
    wordCount "Foo bar baz lala" |> toString |> Html.text
