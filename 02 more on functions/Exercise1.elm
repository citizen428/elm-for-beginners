module Main exposing (..)

import Html
import String exposing (left)


(~=) s1 s2 =
    left 1 s1 == left 1 s2


main =
    "foo" ~= "foo" |> toString |> Html.text
