module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


-- model


type alias Model =
    { total : Int
    , new : Int
    }


initModel : Model
initModel =
    { total = 0
    , new = 0
    }



-- update


type Msg
    = NewCalories String
    | AddCalories
    | Clear


update : Msg -> Model -> Model
update msg model =
    case msg of
        NewCalories calories ->
            let
                newInt =
                    Result.withDefault 0 (String.toInt calories)
            in
                { model | new = newInt }

        AddCalories ->
            { model | total = model.total + model.new, new = 0 }

        Clear ->
            initModel



-- view


view : Model -> Html Msg
view model =
    div []
        [ h3 []
            [ text ("Total Calories: " ++ (toString model.total)) ]
        , input
            [ type_ "number"
            , placeholder "Calories"
            , value (inputValue model.new)
                (if model.new == 0 then
                    ""
                 else
                    toString model.new
                )
            , onInput NewCalories
            ]
            []
        , button
            [ type_ "button"
            , onClick AddCalories
            ]
            [ text "Add" ]
        , button
            [ type_ "button"
            , onClick Clear
            ]
            [ text "Clear" ]
        ]


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initModel
        , update = update
        , view = view
        }
