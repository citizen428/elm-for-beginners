module Main exposing (..)

import Html exposing (text)
import String exposing (length, toUpper)


main =
    let
        name =
            "Michael"

        nameLength =
            length name

        formattedName =
            if nameLength > 10 then
                toUpper name
            else
                name
    in
        text (formattedName ++ " - name length: " ++ (toString nameLength))
